import { eq, either, both, bind, all, any } from "./goal.js"
import { Goal } from "./goal.js"
import Pair from "./pair.js"
import Variable from "./variable.js"

const INC = {
  inspect() {
    return "+"
  },
}

export const ZERO = {
  inspect() {
    return "0"
  },
}

export function fromNumber(n) {
  if (n === 0) return ZERO
  else return new Pair(INC, fromNumber(n - 1))
}

export function toNumber(integer) {
  if (integer === ZERO) return 0
  else return 1 + toNumber(integer.right)
}

export function inc(a, a_add1) {
  return eq(a_add1, new Pair(INC, a))
}

export function add(a, b, c) {
  return either(
    both(eq(a, ZERO), eq(b, c)),
    bind(["x", "y"], (x, y) => both(both(inc(x, a), inc(y, c)), add(x, b, y)))
  )
}

export function subtract(a, b, c) {
  return add(b, c, a)
}

export function lesserOrEqual(a, b) {
  return new Goal((state) => {
    let [aa, bb] = [state.walk(a), state.walk(b)]
    while (!(aa instanceof Variable)) {
      if (aa == ZERO) {
        return (function* () {
          yield state
        })()
      }
      if (bb == ZERO) {
        return (function* () {})()
      }
      if (bb instanceof Variable) {
        console.error(aa, bb)
        throw "unhandled bb"
      }
      aa = aa.right
      bb = bb.right
    }
    return (function* () {
      yield state.assign([aa, bb])
      while (bb != ZERO) {
        bb = bb.right
        yield state.assign([aa, bb])
      }
    })()
  })
}

// alternative impl
// /** Slow */
function lesserOrEqualNaive(a, b) {
  return bind(["b-a"], (ba) => subtract(b, a, ba))
}

function lesserOrEqualNaive2(a, b) {
  return either(
    eq(a, ZERO),
    bind(["x", "y"], (x, y) => all(inc(x, a), inc(y, b), lesserOrEqual(x, y)))
  )
}

export function multiply(a, b, c) {
  return either(
    both(eq(a, ZERO), eq(c, ZERO)),
    bind(["x", "y"], (x, y) =>
      all(inc(x, a), add(y, b, c), lesserOrEqual(a, c), multiply(x, b, y))
    )
  )
}

export function divide(a, b, c) {
  return multiply(b, c, a)
}
