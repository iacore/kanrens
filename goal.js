import * as stream from './stream.js';

export class Goal {
  constructor(func) {
    this._func = func;
  }

  pursue(state) {
    return this._func(state);
  }
}
export default Goal

export function succeed() {
  return new Goal(state => {
    return function*() {
      yield state;
    }();
  });
}

export function fail() {
  return new Goal(state => {
    return function*() {
    }();
  });
}

export function equal(a, b) {
  return new Goal(state => {
    state = state.unify(a, b);

    return function*() {
      if (state) yield state;
    }();
  });
}

export function bind(names, func) {
  return new Goal(state => {
    let [newState, vars] = state.createVars(...names),
        goal = func(...vars);

    return goal.pursue(newState);
  });
}

export function either(a, b) {
  return new Goal(state => {
    return stream.interleave(a.pursue(state), b.pursue(state));
  });
}

export function any(first, ...rest) {
  if (rest.length === 0)
    return first;
  else
    return either(first, any(...rest));
}

export function both(a, b) {
  return new Goal(state => {
    return function*() {
      for (let state2 of a.pursue(state)) {
        for (let result of b.pursue(state2))
          yield result;
      }
    }();
  });
}

export function all(first, ...rest) {
  if (rest.length === 0)
    return first;
  else
    return both(first, all(...rest));
}

export { equal as eq, either as or, both as and };
